package com.usermanagement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBUtils {

    private static String jdbcURL = "jdbc:h2:~/usermanagement";
    private static String jdbcUsername = "sa";
    private static String jdbcPassword = "password";
    static final String JDBC_DRIVER = "org.h2.Driver";
    public static Connection connection = null;

    public static Connection getConnection() {
        try {
            Class.forName(JDBC_DRIVER);
            if(connection == null) {
                connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
            }
            return connection;
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return connection;
    }




    //set up the initial H2 database schema
    public static void initSchema() throws Throwable {
        Connection conn = DBUtils.getConnection();
        Statement statement = conn.createStatement();
        //create session table
        statement.executeUpdate("CREATE TABLE SESSION (\n" +
                "  id VARCHAR(250)  PRIMARY KEY,\n" +
                "  token VARCHAR(250) NOT NULL,\n" +
                "  user_id varchar(250) NOT NULL,\n" +
                "  created_timestamp VARCHAR(255) NOT NULL\n" +
                ")");

        //create users table
        statement.executeUpdate("CREATE TABLE USER (\n" +
                "  uuid VARCHAR(250) PRIMARY KEY,\n" +
                "  name VARCHAR(250) NOT NULL,\n" +
                "  password VARCHAR(250) NOT NULL,\n" +
                "  phone VARCHAR(250) NOT NULL UNIQUE\n" +
                ")");


        //create user session table
        statement.executeUpdate("CREATE TABLE USER_SESSION (\n" +
                "                uuid VARCHAR(250)   PRIMARY KEY,\n" +
                "                user_id VARCHAR(250) NOT NULL,\n" +
                "                session_id VARCHAR(250) NOT NULL,\n" +
                "                created_timestamp VARCHAR(255) NOT NULL\n" +
                "        );");
    }
}
