package com.usermanagement;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Session {

    private String userId;
    private String token;
    private String createdTimeStamp;
}
