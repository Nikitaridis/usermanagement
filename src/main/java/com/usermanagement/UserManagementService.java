package com.usermanagement;



import org.springframework.web.bind.annotation.*;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;


@RestController
    public class UserManagementService {

    //add new users to the system - [username, password,phone]
    //https://localhost:8443/api/addUser?password=pass&name=julius&phone=0834448373
    @RequestMapping(value = "/api/addUser")
    public Map<String, String> addUser(@RequestParam(required = true) String password,
                                       @RequestParam(required = true) String name,
                                       @RequestParam(required = true) String phone) throws Throwable {

        String passwordHash = generateHash(password);
        String userId = UUID.randomUUID().toString();

        String sql = "insert into user(uuid,name,password,phone)" +
                " values('" + userId + "','" + name + "','" + passwordHash + "','" + phone + "');";

        DBUtils.getConnection().createStatement().executeUpdate(sql);
        return new HashMap<String,String>(){{
            put("responseMesssage","user has been added to system "+userId);
        }};
    }


    //list all users in the system
    //add new users to the system - [username, password,phone]
    @RequestMapping(value = "/api/listUsers")
    public List<User> listUsers() throws SQLException {
        List<User> userList = new ArrayList<User>();
        ResultSet rs = DBUtils.getConnection().createStatement().executeQuery("SELECT * FROM USER");

        while (rs.next()) {
            userList.add(new User(rs.getString("uuid"),
                    rs.getString("phone"),rs.getString("name")));
        }
        return userList;
    }



    //list all users that currently have a valid session
    @RequestMapping(value="/api/listActiveUsers")
    public List<User> listUsersWithValidSession() throws SQLException {
        List<User> activeUsers = new ArrayList<>();
        ResultSet rs = DBUtils.getConnection().createStatement().executeQuery("select * from user a , user_session b where b.user_id = a.uuid");
        while(rs.next()) {
            activeUsers.add(new User(rs.getString("user_id"),
                    rs.getString("phone"),rs.getString("name")));
        }
        return activeUsers;
    }

    //list all the valid sessions i.e users that are logged on and authenticared
    @RequestMapping(value="/api/listActiveSessions")
    public List<Session> listActiveSessions() throws SQLException {

        ResultSet rs = DBUtils.getConnection().createStatement().executeQuery("select * from session");
        List<Session> activeSessions = new ArrayList<>();

        while(rs.next()) {
            //check that the session has not timed out
            if(checkSessionTimeout(rs.getString("user_id"))) {
                activeSessions.add(new Session(rs.getString("user_id"),
                        rs.getString("token"),
                        rs.getString("created_timestamp")));
            }
        }

        return activeSessions;
    }




    @RequestMapping(value = "/api/loginUser")
    public HashMap<String,String> loginUser(@RequestParam(required = true) String username,
                                    @RequestParam(required = true) String password) throws Throwable {
        //authenticate user
        String sql = "select * from USER where name='"+username+"'";
        ResultSet rs = DBUtils.getConnection().createStatement().executeQuery(sql);
//        if(rs.getString("name") == null) {
//            //TODO user was not found
//            //throw new Exception("User was not found");
//            return new HashMap<String,String>(){{
//                put("reponseMessage","user was not found in DB");
//            }};
//        }
        String passwordHash = generateHash(password);
        String passwordFromDB = null;
        String userId = null;
        while (rs.next()) {
            passwordFromDB = rs.getString("password");
            userId = rs.getString("uuid");
        }

        if(passwordFromDB.equalsIgnoreCase(passwordHash)) {
            //generate a session and insert into the session table. This is to store a history of all the users sessions
            //sessions will never be removed from the session table.A session will be active when it is accociated to
            //a user id in the user_sessions table
            String sessionId = UUID.randomUUID().toString();
            String userToken = UUID.randomUUID().toString();
            Long currentTimestamp = new Date().getTime();
            Statement stmt = DBUtils.getConnection().createStatement();
            stmt.executeUpdate("insert into session(id,token,user_id,created_timestamp) " +
                    "values('"+sessionId+"','"+userToken+"','"+userId+"','"+currentTimestamp+"')");
            associateSessionToUser(userId,sessionId);
            //create response
            HashMap<String, String> responseValues = new HashMap<String, String>();
            responseValues.put("id",userId);
            responseValues.put("token",userToken);
            return responseValues;
        } else {
            //TODO password for the user is not correct
            return null;
        }
    }



    /*
        Once a user has been authenticated, and a session created, this session must be associated to a user id
        in the user_session table. all rows in this table will represent all users in the system that have a valid
        session.
     */
    public void associateSessionToUser(String userId, String sessionId) throws SQLException {
        String sql = "insert into user_session(uuid,user_id,session_id,created_timestamp) " +
                "values ('"+UUID.randomUUID()+"','"+userId+"','"+sessionId+"','"+new Date().getTime()+"')";
        DBUtils.getConnection().createStatement().executeUpdate(sql);
    }




    //this is a method that should be called to see if a users session is still valid
    @RequestMapping(path = "/api/checkSessionTimeout")
    public boolean checkSessionTimeout(@RequestParam(required = true) String userId) throws SQLException {
        Long currentTimestamp = new Date().getTime();

        Connection conn = DBUtils.getConnection();
        ResultSet rs = conn.createStatement().executeQuery("select * from user_session where user_id = '"+userId+"'");
        Long sessionTimestamp = 9999999L;
        while(rs.next()) {
            sessionTimestamp = Long.valueOf(rs.getString("created_timestamp"));
        }

        if(currentTimestamp - sessionTimestamp < 180000) {
            return true; //session has been active for more than 3 mins
        } else {
            String sql  = "delete from user_session where user_id = '"+userId+"'";
            conn.createStatement().executeUpdate(sql);
            return false;
        }
    }




    @RequestMapping(value = "/api/logoutUser")
    public HashMap<String, String> logoutUser(@RequestParam(required = true) String userId) throws SQLException {
        String sql = "select * from user_session where user_id = '"+userId+"'";
        ResultSet rs = DBUtils.getConnection().createStatement().executeQuery(sql);
        String sessionId = null;
        while(rs.next()) {
            sessionId = rs.getString("session_id");
        }

        //get the token from session table
        sql = "select * from session where id = '"+sessionId+"'";
        DBUtils.getConnection().createStatement().executeQuery(sql);
        String sessionToken = null;
        while(rs.next()) {
            sessionToken = rs.getString("token");
        }

        sql = "delete from user_session where user_id = '"+userId+"'";
        DBUtils.getConnection().createStatement().executeUpdate(sql);
        HashMap<String,String> results = new HashMap<String,String>();
        results.put("token",sessionToken);

        return results;
    }



    public String generateHash(String message) throws Throwable {
        MessageDigest digest = MessageDigest.getInstance("MD5");
        byte[] hashedBytes = digest.digest(message.getBytes("UTF-8"));
        return convertByteArrayToHexString(hashedBytes);
    }


    private static String convertByteArrayToHexString(byte[] arrayBytes) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < arrayBytes.length; i++) {
            stringBuffer.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return stringBuffer.toString();
    }



    @RequestMapping(path = "/api/initDb")
    public Map<String,String> setupDB() throws Throwable {
        DBUtils.initSchema();
        return new HashMap<String,String>() {{
            put("responseMessage", "H2 DB schema has been set up");
        }};
    }
}
