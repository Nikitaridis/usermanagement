package com.usermanagement;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class User {

    private String userId;
    private String phone;
    private String userName;
}
