###H2 datasource configuration 
The first time this application is run, the webservice endpoint
[InitDb](https://localhost:8443/api/initDB) should be invoked.This will create the needed H2 tables.
A file usermanagement should be created in the users home dir on linux hosts.

once the script h2.sh has been invoked from the H2/bin directory, the usermanagement
database can be configured and created with http://localhost:8082
* username : sa
* password: password

application.properties configuration 
`spring.datasource.url=jdbc:h2:~/usermanagement
 spring.datasource.driverClassName=org.h2.Driver
 spring.datasource.username=sa
 spring.datasource.password=password
 spring.jpa.database-platform=org.hibernate.dialect.H2Dialect`



###SSL configuration 
In SOAPUI settings the java keystore **keystore.p12** should be added as all webservice 
endpoints are exposed on port 8443 with TLS encryption.The keystore contains 
self signed RSA certs.
* keystore password: changeit

application.properties config

`server.port=8443
 server.ssl.key-store-type=PKCS12
 server.ssl.key-store=keystore.p12
 server.ssl.key-store-password=changeit
 server.ssl.key-alias=tomcat
 security.require-ssl=true`


###SOAPUI Rest Client
The SOAP UI project **REST-UserManagement**, in the project root should be imported into SOAP UI.
This project included example requests for all the webservice endpoins

 


###Webservices listing  
* /api/listUsers
* /api/addUser
* /api/loginUser
* /api/logoutUser
* /api/listActiveUsers
* /api/checkSessionTimeout
* /api/ListActiveSessions
* /api/initDB
